import { Request } from "./requests-url.js";
import { getToken, cardRender } from "./login-form.js";


export const selectDoctor = document.querySelector("#doctor");
const visitModal = document.querySelector("#visit-modal");
export const visitBtn = document.querySelector("#create-visit-btn");
const closeVisitModal = document.querySelector("#close-visit-modal");
const submitBtn = document.querySelector("#create-visit");
const hiddenModal = "hidden";

export function createVisitModal() {
  visitModal.classList.toggle(hiddenModal);
}

export function closeCreateVisitModal () {

    closeVisitModal.addEventListener("click", (e) => {
        visitModal.classList.toggle(hiddenModal);
    });

    document.addEventListener('click', (e)=> {
        if(e.target.classList.contains('create-visit')) {
            visitModal.classList.toggle(hiddenModal);
            clearInputs();
        }
    })
}
// closeVisitModal.addEventListener("click", (e) => {
//   visitModal.classList.toggle(hiddenModal);
// });

function clearInputs() {
  const formModal = document.querySelector('#create-visit-card');
  formModal.querySelectorAll("input").forEach((el) => (el.value = ""));
}

// зміна контексту Модалки в залежності від лікаря
export function selectDoctorModal() {

    selectDoctor.addEventListener("change", () => {
        const optionalInputs = document.querySelector(".optional-inputs");
        optionalInputs.innerHTML = "";
      
        if (selectDoctor.value === 'Therapist') {
          optionalInputs.innerHTML = `<div>
          <p class="form-modal__label">Age*</p>
          <input type="number" id="age" name="age" class="input input__visit" placeholder="Enter patient age" required>
        </div>`;
      
        } else if (selectDoctor.value === 'Cardiologist') {
          optionalInputs.innerHTML = `<div class="input__visit">
          <div>
              <p class="form-modal__label">Age*</p>
              <input type="number" id="age" name="age" class="input input__visit" placeholder="Enter patient age" required>
          </div>
          <div>
              <p class="form-modal__label">Cardiovascular diseases*</p>
              <input type="text" id="illinesse" name="illinesse" class="input input__visit" placeholder="Enter diseases" required>
          </div>
        </div>
        <div class="input__visit">
          <div>
              <p class="form-modal__label">Normal pressure (mmHg)*</p>
              <input type="text" id="pressure" name="pressure" class="input input__visit" placeholder="Enter patient normal pressure" required>
          </div>
          <div>
              <p class="form-modal__label">Body mass index*</p>
              <input type="number" id="bmi" name="bmi" class="input input__visit" placeholder="Enter patient mass index" required>
          </div>
        </div>`;
      
        } else if (selectDoctor.value === 'Dentist') {
          optionalInputs.innerHTML = `<div>
          <p class="form-modal__label">Last visit date*</p>
          <input type="date" id="lastVisitDate" name="lastVisitDate" class="input input__visit" placeholder="mm/dd/yyyy" required>
        </div>`
          }
      });
}


// Відправити дані
export async function submitCreateVisit() {

    submitBtn.addEventListener("click", async ()=> {
        const formModal = document.querySelector('#create-visit-card');
        const firstName = formModal.querySelector('#firstName').value;
        const lastName = formModal.querySelector('#lastName').value;
        const doctor = formModal.querySelector("#doctor").value;
        const purpose = formModal.querySelector('#purpose').value;
        const description = formModal.querySelector('#description').value;
        const priority = formModal.querySelector('#priority').value;
        const status = "Awaiting";
       
        try {
            let body;
            if (doctor === "Dentist") {
              const lastVisitDate = formModal.querySelector("#lastVisitDate").value;
              body = JSON.stringify({
                firstName,
                lastName,
                doctor,
                purpose,
                description,
                priority,
                status,
                lastVisitDate,
              });
            } else if (doctor === "Therapist") {
              const age = formModal.querySelector("#age").value;
              body = JSON.stringify({
                firstName,
                lastName,
                doctor,
                purpose,
                description,
                priority,
                status,
                age,
              });
            } else if (doctor === "Cardiologist") {
              const pressure = formModal.querySelector("#pressure").value;
              const bmi = formModal.querySelector("#bmi").value;
              const illnesse = formModal.querySelector("#illinesse").value;
              const age = formModal.querySelector("#age").value;
              body = JSON.stringify({
                firstName,
                lastName,
                doctor,
                purpose,
                description,
                priority,
                status,
                pressure,
                bmi,
                illnesse,
                age,
              });
            }
            const response = await fetch(
              "https://ajax.test-danit.com/api/v2/cards",
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                  Authorization: `Bearer ${getToken()}`,
                },
                body,
              }
            );
            const data = await response.json();
            clearInputs();
            createVisitModal();
            cardRender(data);

          } catch (error) {
            console.log(error);
          }
    });
}

