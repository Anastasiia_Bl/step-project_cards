import { Request } from "./requests-url.js";
import { getToken } from './login-form.js';
import { visitCard } from "./visitCard.js";
import { cardRender } from "../app.js";

const mainHtml = document.querySelector('main')

export class ModalEdit {
    constructor(body, id) {
        this.body = body;
        this.id = id;

    }
    createModal(){
        this.modal = document.createElement('div');
        this.modal.classList.add('modal')
        this.modal.insertAdjacentHTML('beforeend', `<div class="modal-body">
            <div class="header-modal">
            <h3 class="header-modal__title">Edit Visit card</h3>
            <button class="edit__btn-close button button-secondary">Close</button>
            </div>
            <div class="edit__body"></div>
            <div class="edit__btn-wrapper"><button class="edit__btn-submit button button-primary" type="button">Submit</button></div>
        </div>`)
        const modalBody = this.modal.querySelector('.edit__body');
        if(typeof(this.body) === 'string') {
            modalBody.insertAdjacentHTML('beforeend', `${this.body}`)
        } else {
            modalBody.insertAdjacentElement('beforeend', this.body)
        }
        this.closeModal();
        mainHtml.insertAdjacentElement('beforeend', this.modal)
    }

    closeModal(){
        this.modal.addEventListener('click', e => {
            const modalClose = e.target.closest('.edit__btn-close');
            if(modalClose) this.modal.remove();
        })
        this.modal.addEventListener('click', e=> {
            if(e.target.classList.contains('modal')) this.modal.remove();
        })
    }

    getSubmitBtn() {
        return this.modal.querySelector('.edit__btn-submit');
      }
    getEditBody() {
        return this.modal.querySelector('.edit__body');
    }
   
}