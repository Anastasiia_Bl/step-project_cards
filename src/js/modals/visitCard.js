import { Request } from "./requests-url.js";
import { ModalEdit } from "./modalEdit.js";
import { cardRender, cardsArray } from "./login-form.js";

export class visitCard {
    constructor({firstName, lastName, doctor, purpose, description, priority, id, status}) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.doctor = doctor;
      this.id = id;
      this.purpose = purpose;
      this.description = description;
      this.priority = priority;
      this.status = status;
    }
    createHTMLCard() {
      const template = document.querySelector('#card-template');
		  const card = template.content.cloneNode(true).querySelector('.card-visit');
		  card.querySelector('.card-visit__id').textContent = this.id;
		  card.querySelector('.card-visit__doctor').textContent = this.doctor;
		  card.querySelector('.card-visit__priority').textContent = this.priority;
      card.querySelector('.card-visit__first-name').textContent = this.firstName;
      card.querySelector('.card-visit__last-name').textContent = this.lastName;
      card.querySelector('.card-visit__status').textContent = this.status;
		  card.dataset.id = this.id;

      // перевірка статусу картки
      if (this.status === 'Awaiting') {
        card.querySelector('.card-visit__status').style.background = '#1F78D1';
      } else if (this.status === 'Done') {
        card.querySelector('.card-visit__status').style.background = '#1AAA55';
      }
      
		return card;
    }
  
    deleteCard(currentCard) {
      let cardsSection = document.querySelector('.cards-section');
      console.log(cardsArray)

      currentCard.querySelector('.card-visit__btn-delete').addEventListener('click', () => {
        Request.deleteCard(this.id).then(response => {
          if(response.ok) {
          currentCard.remove();
          }
      cardsArray.find((el, index) => {
        if (el.id === this.id) {
          cardsArray.splice(index, 1);
        }
      });
      if(cardsArray.length === 0) {
        cardsSection.classList.add('hidden');
        document.querySelector('.authorise-block').classList.remove('hidden');
      }
    })
  })
}
  
    addAdditionalHTMLInfo() {
      return `<p class="card-visit__title">Patient's visit purpose</p>
              <p class="card-visit__title">Description</p>
              <p class="card-visit__purpose input">${this.purpose}</p>
              <p class="card-visit__description input">${this.description}</p>`
    }

    showAdditionalInfo(currentCard) {
      currentCard.querySelector('.card-visit__btn-show').addEventListener('click', (event)=> {
        const cardId = currentCard.getAttribute('data-id');
        const additionalBlock = currentCard.querySelector(`[data-id="${cardId}"] .card-visit__additional-info`);

      additionalBlock.classList.toggle('hidden');
      let showButton = currentCard.querySelector('.card-visit__btn-show');
      if (!additionalBlock.classList.contains('hidden')) {
        let additionalHTMLInfo = this.addAdditionalHTMLInfo();
        additionalBlock.insertAdjacentHTML('beforeend', additionalHTMLInfo);
        showButton.textContent = 'Show less';
      } else {
        additionalBlock.innerHTML = '';
        showButton.textContent = 'Show more';
      }
      })
    }

    addEditHTMLInfo() {
      return `<select name="priority" class="dropdown edit__priority">
                <option value="Low priority" class="dropdown-low">Low priority</option>
                <option value="Medium priority" class="dropdown-medium">Medium priority</option>
                <option value="High priority" class="dropdown-high">High priority</option>
              </select>
              <select name="status" class="dropdown edit__status">
                <option value="Awaiting" class="status-awaiting">Awaiting</option>
                <option value="Done" class="status-done">Done</option>
              </select>
              <label class="edit__label" for="first-name">First Name
                <input class ='input edit__first-name' type="text" name="first-name" value="${this.firstName}"></label>
              <label class="edit__label" for="last-name">Last Name
                <input class ='input edit__last-name' type="text" name="last-name" value="${this.lastName}"></label>
              <label class="edit__label" for="purpose">Patient's visit purpose
                <input class ='input edit__purpose' type="text" name="purpose" value="${this.purpose}"></label>
              <label class="edit__label" for="description">Description
                <input class ='input edit__description' type="text" name="description" value="${this.description}"></label>
              <label class="edit__label" for="doctor">Doctor
                <input class ='input edit__doctor' type="text" name="doctor" disabled value="${this.doctor}"></label>`
    }

    editCard(currentCard) {
      currentCard.querySelector('.card-visit__btn-edit').addEventListener('click', (e)=> {
        const modalEdit = new ModalEdit(this.addEditHTMLInfo(), this.id);
        modalEdit.createModal();

        const submitButton = modalEdit.getSubmitBtn();
        const modalBody = modalEdit.getEditBody();
        const doctor = modalBody.querySelector('.edit__doctor').value;
        const id = this.id;

        submitButton.addEventListener('click', () => {
          
                  let updatedCard = {};
            if (doctor === "Cardiologist") {
                const firstName = modalBody.querySelector('.edit__first-name').value;
                const lastName = modalBody.querySelector('.edit__last-name').value;
                const purpose = modalBody.querySelector('.edit__purpose').value;
                const description = modalBody.querySelector('.edit__description').value;
                const pressure = modalBody.querySelector('.edit__pressure').value;
                const bmi = modalBody.querySelector('.edit__bmi').value;
                const illnesse = modalBody.querySelector('.edit__illnesse').value;
                const age = modalBody.querySelector('.edit__age').value;
                let priority = modalBody.querySelector('.edit__priority').value;
                let status = modalBody.querySelector('.edit__status').value;
                    updatedCard = {
                      firstName,
                      lastName,
                      doctor,
                      purpose,
                      description,
                      priority,
                      id,
                      status,
                      pressure,
                      bmi,
                      illnesse,
                      age,
                    };
                  } else if (doctor === "Dentist") {
                    const firstName = modalBody.querySelector('.edit__first-name').value;
                    const lastName = modalBody.querySelector('.edit__last-name').value;
                    const purpose = modalBody.querySelector('.edit__purpose').value;
                    const description = modalBody.querySelector('.edit__description').value;
                    const lastVisitDate = modalBody.querySelector('.edit__last-visit-date').value;
                    let priority = modalBody.querySelector('.edit__priority').value;
                    let status = modalBody.querySelector('.edit__status').value;
                    updatedCard = {
                        firstName,
                        lastName,
                        doctor,
                        purpose,
                        description,
                        priority,
                        id,
                        status,
                        lastVisitDate,
                    };
                  } else if (doctor === "Therapist") {
                    const firstName = modalBody.querySelector('.edit__first-name').value;
                    const lastName = modalBody.querySelector('.edit__last-name').value;
                    const purpose = modalBody.querySelector('.edit__purpose').value;
                    const description = modalBody.querySelector('.edit__description').value;
                    const age = modalBody.querySelector('.edit__age').value;
                    let priority = modalBody.querySelector('.edit__priority').value;
                    let status = modalBody.querySelector('.edit__status').value;
                    updatedCard = {
                        firstName,
                        lastName,
                        doctor,
                        purpose,
                        description,
                        priority,
                        id,
                        status,
                        age,
                    };
                  }
                  console.log(updatedCard);
                  Request.editCard(this.id, updatedCard)
                  .then(response => {
                    for (let key in this) {
                      this[key] = response[key];
                    }
                  const newCard = this.createHTMLCard();
                  console.log(newCard)
                  const oldCard = document.querySelector(`[data-id="${this.id}"]`);
                  oldCard.replaceWith(newCard);
                  this.deleteCard(newCard);
                  this.showAdditionalInfo(newCard);
                  this.editCard(newCard);
                  modalEdit.modal.style.display = "none";
                  })
        });
      })

      }
    }


export class visitCardCardiologist extends visitCard {
  constructor(firstName, lastName, doctor, purpose, description, priority, id, status, pressure, bmi, illnesse, age) {
    super({firstName, lastName, doctor, purpose, description, priority, id, status});
    this.pressure = pressure;
    this.bmi = bmi;
    this.illnesse = illnesse;
    this.age = age;
  }

  addAdditionalHTMLInfo() {
    return `${super.addAdditionalHTMLInfo()}
    <p class="card-visit__title">Age</p>
    <p class="card-visit__title">Сardiovascular diseases</p>
    <p class="card-visit__age input">${this.age}</p>
    <p class="card-visit__illnesse input">${this.illnesse}</p>
    <p class="card-visit__title">Normal pressure (mmHg)</p>
    <p class="card-visit__title">Body mass index</p>
    <p class="card-visit__ input">${this.pressure}</p>
    <p class="card-visit__bmi input">${this.bmi}</p>`
  }

  addEditHTMLInfo() {
    return `${super.addEditHTMLInfo()}
    <label class="edit__label" for="age">Age
			<input class ='input edit__age' type="text" name="age" value="${this.age}"></label>
		<label class="edit__label" for="illnesse">Сardiovascular diseases
			<input class ='input edit__illnesse' type="text" name="illnesse" value="${this.illnesse}"></label>
		<label class="edit__label" for="pressure">Normal pressure (mmHg)
			<input class ='input edit__pressure' type="text" name="pressure" value="${this.pressure}"></label>
		<label class="edit__label" for="bmi">Body mass index
			<input class ='input edit__bmi' type="text" name="bmi" value="${this.bmi}"></label>
    
    `
  }
}



export class visitCardTherapist extends visitCard {
  constructor(firstName, lastName, doctor, purpose, description, priority, id, status, age) {
      super({firstName, lastName, doctor, purpose, description, priority, id, status});
      this.age = age;
  }

  addAdditionalHTMLInfo() {
      return `${super.addAdditionalHTMLInfo()}
      <p class="card-visit__title">Age</p><br>
      <p class="card-visit__age input">${this.age}</p>`
  }

  addEditHTMLInfo() {
    return `${super.addEditHTMLInfo()}
    <label class="edit__label" for="age">Age
			<input class ='input edit__age' type="text" name="age" value="${this.age}"></label>
    `
  }
}


export class visitCardDentist extends visitCard {
  constructor(firstName, lastName, doctor, purpose, description, priority, id, status, lastVisitDate) {
    super({firstName, lastName, doctor, purpose, description, priority, id, status});
    this.lastVisitDate = lastVisitDate;
  }

  addAdditionalHTMLInfo() {
    return `${super.addAdditionalHTMLInfo()}
    <p class="card-visit__title">Last visit date</p><br>
    <p class="card-visit__last-visit-date input">${this.lastVisitDate}</p>`
  }

  addEditHTMLInfo() {
    return `${super.addEditHTMLInfo()}
    <label class="edit__label" for="last-visit-date">Last visit date
			<input class ='input edit__last-visit-date' type="text" name="last-visit-date" value="${this.lastVisitDate}"></label>
      `
  }
}


// метод відалення карток, написаний одразу, не виконується умова динамічно
// deleteCard(currentCard) {
//   // const deleteBtn = currentCard.querySelector('.card-visit__btn-delete');
//   let cardsSection = document.querySelector('.cards-section');

//   currentCard.querySelector('.card-visit__btn-delete').addEventListener('click', () => {
//     Request.deleteCard(this.id).then(response => {
//       if(response.ok) {
//         currentCard.remove();
//         if(!cardsSection.children.length) {
//           cardsSection.classList.add('hidden');
//           document.querySelector('.authorise-block').classList.remove('hidden');
//         }
//       }
      
//     })
//   })
// }