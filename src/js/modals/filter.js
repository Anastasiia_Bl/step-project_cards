import { cardsArray, visitCardsInit } from "./login-form.js";

// Фільтрація карток
export const formFilter = document.querySelector('.filter');
const purposeInput = document.querySelector('.input__filter');
const statusInput = document.querySelector('.status');
const priorityInput = document.querySelector('.priority');


//фільтер працює по одному значенню 
export function filterCards() {
  let noVisits = document.querySelector(".no-items");
  if (noVisits) {
    noVisits.remove();
  }

  if (!purposeInput.value && statusInput.value === "all" && priorityInput.value === "all") {
    // если все инпуты пустые, и none, то показываем все карточки
    cardsArray.forEach((card) => {
      const cardElem = document.querySelector(`[data-id="${card.id}"]`);
      cardElem.classList.remove("hidden");
    });
    return;
  }

  cardsArray.forEach((card) => {
    let matchCount = 0;
    if (purposeInput.value.trim() && (card.purpose.toLowerCase().includes(purposeInput.value.toLowerCase()) || card.description.toLowerCase().includes(purposeInput.value.toLowerCase()))) {
      matchCount++;
    }
    if (statusInput.value && card.status.toLowerCase() === statusInput.value.toLowerCase()) {
      matchCount++;
    }
    if (priorityInput.value && card.priority.toLowerCase() === priorityInput.value.toLowerCase()) {
      matchCount++;
    }
    const cardElem = document.querySelector(`[data-id="${card.id}"]`);
    if (matchCount === 0) {
      cardElem.classList.add("hidden");
    } else {
      cardElem.classList.remove("hidden");
    }
  });
  let cardsSection = document.querySelector('.cards-section');
  let allItems = cardsSection.querySelectorAll(".card-visit:not(.hidden)");
  if (!allItems.length) {
    cardsSection.insertAdjacentHTML(
      "afterbegin",
      `<div class="no-items">No visits found</div>`
    );
  }
}

formFilter.addEventListener('change', filterCards);
formFilter.addEventListener('input', filterCards);





// clear filter
// export function clearFilter() {
//   purposeInput.value = "";
//   statusInput.value = "";
//   priorityInput.value = "";
//   filterCards();
// }
