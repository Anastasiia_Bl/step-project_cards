// импортировать все функции с functions.js
import * as flsFunctions from "./gulp/functions.js";
flsFunctions.isWebp();

import { closeModalForm, openLoginForm, changeType, checkAuthorization, validateLogin, login, logInBtnClick, getToken} from "./modals/login-form.js";
import { formFilter, filterCards } from "./modals/filter.js";
import { visitBtn, createVisitModal, selectDoctorModal, submitCreateVisit, closeCreateVisitModal } from "./modals/createVisit.js";


// Логінізація + ініціалізація і рендер карток при успішній логінізації
closeModalForm();
openLoginForm();
// Показати, сховати значення Пароля
let showPass = document.querySelectorAll(".password__btn");
showPass.forEach(item =>
  item.addEventListener('click', changeType)
);

checkAuthorization();
logInBtnClick();

// Фільтр
formFilter.addEventListener('change', filterCards);
formFilter.addEventListener('input', filterCards);

// Create Visit
visitBtn.addEventListener("click", createVisitModal);
closeCreateVisitModal();
selectDoctorModal();
submitCreateVisit();

