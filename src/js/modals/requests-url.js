import { getToken } from './login-form.js';

export class Request {
  constructor() {}

  static getAllCards() {
    return fetch(`https://ajax.test-danit.com/api/v2/cards`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
    }).then((response) => response.json());
  }

  static getCard(id) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
    }).then((response) => response.json());
  }
  static deleteCard(id) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
  }

  static createCard(body) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(body),
    }).then((response) => response.json());
  }

  static editCard(id, cardBody) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(cardBody),
    }).then((response) => response.json());
  }
}
// fetch("https://ajax.test-danit.com/api/v2/cards", {
//   method: 'POST',
//   headers: {
//     'Content-Type': 'application/json',
//     'Authorization': `Bearer ${getToken()}`
//   },
//   body: JSON.stringify({
//     firstName: 'Kolya',
//     lastName: 'Ivanov',
//     purpose: 'Visit to Cardiologist',
//     description: 'Plan visit',
//     doctor: 'Cardiologist',
//     priority: 'Low priority',
//     status: 'Awaiting',
//     pressure: '120/80',
//     bmi: '20',
//     illnesse: 'Tahicardia',
//     age: '24',
//   })
// })
//   .then(response => response.json())
//   .then(response => console.log(response))

// fetch("https://ajax.test-danit.com/api/v2/cards", {
//   method: 'POST',
//   headers: {
//     'Content-Type': 'application/json',
//     'Authorization': `Bearer ${getToken()}`
//   },
//   body: JSON.stringify({
//     firstName: 'Olena',
//     lastName: 'Ivanova',
//     purpose: 'Visit to Dentist',
//     description: 'Plan visit',
//     doctor: 'Dentist',
//     priority: 'Low priority',
//     status: 'Awaiting',
//     lastVisitDate: '12.03.2023',
//   })
// })
//   .then(response => response.json())
//   .then(response => console.log(response))